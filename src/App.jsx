import { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";

axios.defaults.baseURL = 'https://radshopserver-v1.onrender.com/api/products';

function App() {

  const [products, setProducts] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [name, setName] = useState('');
  const [category, setCategory] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');
  const [id, setId] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [cartItems, setCartItems] = useState([]);

  const getAllProducts = async () => {
    try {
      const { data } = await axios.get("/get-product");
      setProducts(data);
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getAllProducts();
  }, []);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const handleImageChange = (event) => {
    setImage(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleAdd = async (event) => {
    event.preventDefault;
    await axios.post("/add-product", {
      name: name,
      image: image,
      description: description,
      category: category,
      price: 9.99
    });
    alert('Product successfully added!');
  };

  const handleEdit = (product) => {
    setIsEditing(true);
    setName(product.name);
    setCategory(product.category);
    setDescription(product.description);
    setImage(product.image);
    setId(product._id);
  };

  const handleDelete = async(product) => {
    handleEdit(product);
    setIsEditing(false);
    await axios.post("delete-product", {
      productId: id,
    });
    alert('Product successfully deleted!');
    setId('');
    setName('');
    setCategory('');
    setDescription('');
    setImage('');
    setIsEditing(false);
  };

  const handleUpdate = async() => {
    await axios.put("edit-product", {
      productId: id,
      name: name,
      category: category,
      description: description,
      image: image,
    });
    alert('Product successfully updated!');
    setIsEditing(false);
  };

  const handleReset = () => {
    setId('');
    setName('');
    setCategory('');
    setDescription('');
    setImage('');
    setIsEditing(false);
  };

  const handleInvoice = (product) => {
    setCartItems([...cartItems, {
      ...product, quantity: 1
    }]);

      // let duplicate = cartItems.find((item) => item.id === product.id);
      // console.log(duplicate);
    // if (cartItems.length === 0) {
    //   setCartItems([...cartItems, {
    //     ...product, quantity: 1
    //   }]);
    // } else {
    //   let duplicate = cartItems.find((item) => item.id === product.id);
    //   console.log(duplicate);
    // }

    // if (duplicate) {
    //   let newCartItems = cartItems.map(({quantity, ...item}) => ({
    //     ...item, quantity: item.id === product.id ? quantity + 1 : quantity,
    //   }));

    //   setCartItems(newCartItems);
    // }

    // setCartItems([...cartItems, {
    //   ...product, quantity: 1
    // }]);
  };

  const handleCartReset = () => {
    setCartItems([]);
  };

  const productsPerPage = 8;
  const pagesVisited = pageNumber * productsPerPage;

  const displayProducts = products.slice(pagesVisited, pagesVisited + productsPerPage).map((product) => {
    return (
      <article 
        key={product._id} className="article"
      >
        <div className="img-container">
          <img src={product.image} alt={product.name} />
        </div>
        <div className="text-container">
          <div className="text-flex">
            <h2 className="article-title">{product.name}</h2>
          </div>
          <span className="article-price">${product.price}</span>
          <p className="article-detail">{product.description}</p>
        </div>
        <button 
          className="edit-btn"
          type="button"
          onClick={() => {
            handleEdit(product);
          }}
        >
          Edit
        </button>
        <button 
          className="edit-btn delete-btn"
          type="button"
          onClick={() => {
            handleDelete(product);
          }}
        >
          Delete
        </button>
        <button 
          className="edit-btn add-btn"
          type="button"
          onClick={() => {
            handleInvoice(product);
          }}
        >
          Add
        </button>
      </article>
    )
  });
  
  const pageCount = Math.ceil(products.length / productsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  }

  return (
    <div className="app">
      <nav className="nav">
        <h1 className="logo">RADHSHOP</h1>
      </nav>
      <div className="giant-flex">
        <div className="flex-container">
          {displayProducts}
          <ReactPaginate
            previousLabel={'P'}
            nextLabel={'N'}
            pageCount={pageCount}
            onPageChange={changePage}
            containerClassName={'pagination'}
            previousClassName={'prev-btn'}
            nextLinkClassName={'next-btn'}
            disabledClassName={'disabled-btn'}
            activeClassName={'active-btn'}
          />
        </div>
        <div className="small-flex">
          <div className="invoice-container">
            <span className="form-header">Invoice Calculator</span>
            <table className="invoice-table">
              <thead>
                <tr className="padded-head-row">
                  <td>Image</td>
                  <td className="invoice-name">Name</td>
                  <td>Price</td>
                  <td>Q ty</td>
                </tr>
              </thead>
              <tbody className="table-body">
                {
                  cartItems.map((item) => (
                    <tr className="padded-row">
                      <td>
                        <img className="invoice-thumbnail" src={item.image} alt={item.name} />
                      </td>
                      <td className="invoice-name">{item.name}</td>
                      <td>${item.price}</td>
                      <td>{item.quantity}</td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
            <div className="invoice-flex">
              <button className="save-btn" onClick={handleCartReset}>Clear Cart</button>
              <h2 className="subtotal">Subtotal: ${cartItems.length === 0 ? '0' : `${cartItems.map((item) => item.price).reduce((total, price) => total + price).toFixed(2)}`}</h2>
            </div>
          </div>
          <div className="form-container">
            <span className="form-header">Add or Edit Product</span>
            <form 
              action="" 
              className="form"
            >
              <div className="form-item">
                <label htmlFor="name">id</label>
                <input 
                  type="text" 
                  value={id}
                  onChange={handleNameChange}
                  disabled
                />
              </div>
              <div className="form-item">
                <label htmlFor="name">Name:</label>
                <input 
                  type="text" 
                  value={name}
                  onChange={handleNameChange}
                />
              </div>
              <div className="form-item">
                <label htmlFor="category">Category:</label>
                <input 
                  type="text" 
                  value={category}
                  onChange={handleCategoryChange}
                />
              </div>
              <div className="form-item">
                <label htmlFor="image">Image:</label>
                <input 
                  type="text" 
                  value={image}
                  onChange={handleImageChange}
                />
              </div>
              <div className="form-item">
                <label htmlFor="description">Description:</label>
                <textarea 
                  name="description" 
                  id="" cols="30" rows="3" 
                  style={{ resize: 'none'}} 
                  onChange={handleDescriptionChange}
                  value={description}
                />
              </div>
              <div className="form-item">
                <button 
                  className="save-btn update-btn"
                  onClick={handleUpdate}
                  type="button"
                  disabled={!isEditing}
                >
                  Update
                </button>
                <button 
                  className="save-btn"
                  onClick={handleAdd}
                  type="button"
                  disabled={isEditing}
                >
                  Add 
                </button>
                <button 
                  className="save-btn"
                  onClick={handleReset}
                  type="button"
                >
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App
